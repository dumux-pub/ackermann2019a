// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup DropModels
 * \brief Element-wise calculation of the residual for problems
 *        using the drop models.
 */
#ifndef DUMUX_DROP_LOCAL_RESIDUAL_HH
#define DUMUX_DROP_LOCAL_RESIDUAL_HH

#include <dumux/common/properties.hh>

namespace Dumux {

/*!
 * \ingroup DropModels
 * \brief Element-wise calculation of the residual for problems
 *        using the drop models.
 */
template<class TypeTag>
class DropLocalResidual : public GetPropType<TypeTag, Properties::BaseLocalResidual>
{
    using ParentType = GetPropType<TypeTag, Properties::BaseLocalResidual>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using ElementFluxVariablesCache = typename GetPropType<TypeTag, Properties::GridFluxVariablesCache>::LocalView;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::FVGridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Element = typename GridView::template Codim<0>::Entity;
    using EnergyLocalResidual = GetPropType<TypeTag, Properties::EnergyLocalResidual>;

    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    static constexpr int numPhases = ModelTraits::numFluidPhases();
    static constexpr int numComponents = ModelTraits::numFluidComponents();
    static constexpr int conti0EqIdx = ModelTraits::Indices::conti0EqIdx; //!< first index for the mass balance
    static constexpr bool useMoles = ModelTraits::useMoles();
    static constexpr int replaceCompEqIdx = ModelTraits::replaceCompEqIdx();
    static constexpr bool useTotalMoleOrMassBalance = replaceCompEqIdx < numComponents;
#if NONISOTHERMAL
    static constexpr int energyEqIdx = ModelTraits::Indices::energyEqIdx;
#endif

public:
    using ParentType::ParentType;

    /*!
     * \brief Evaluatex the rate of change of all conservation
     *        quantites (e.g. phase mass) within a sub-control
     *        volume of a finite volume element for the immiscible models.
     *
     * \param problem The problem
     * \param scv The sub control volume
     * \param volVars The current or previous volVars
     * \note This function should not include the source and sink terms.
     * \note The volVars can be different to allow computing
     *       the implicit euler time derivative here
     */
    NumEqVector computeStorage(const Problem& problem,
                               const SubControlVolume& scv,
                               const VolumeVariables& volVars) const
    {
        NumEqVector storage(0.0);

        const auto massOrMoleDensity = [](const auto& volVars, const int phaseIdx)
        { return useMoles ? volVars.molarDensity(phaseIdx) : volVars.density(phaseIdx); };

        const auto massOrMoleFraction= [](const auto& volVars, const int phaseIdx, const int compIdx)
        { return useMoles ? volVars.moleFraction(phaseIdx, compIdx) : volVars.massFraction(phaseIdx, compIdx); };

        // get current area fractions a_drop and a_g
        std::vector<Scalar> areaFractions(2);
        areaFractions[0] = problem.couplingManager().dropManager().getAreaFraction(scv.dofIndex()); // a_drop for phase 0 (liquid)
        areaFractions[1] = volVars.porosity() - areaFractions[0]; // a_g for phase 1 (gas)

        // compute storage term of all components within all phases
        for (int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
        {
            for (int compIdx = 0; compIdx < numComponents; ++compIdx)
            {
                auto eqIdx = conti0EqIdx + compIdx;
                if (eqIdx != replaceCompEqIdx)
                    storage[eqIdx] += massOrMoleDensity(volVars, phaseIdx)
                                    * volVars.porosity()
                                    * massOrMoleFraction(volVars, phaseIdx, compIdx)
                                    * volVars.saturation(phaseIdx);
            }

            // in case one balance is substituted by the total mole balance
            if (useTotalMoleOrMassBalance)
                storage[replaceCompEqIdx] += massOrMoleDensity(volVars, phaseIdx)
                                           * volVars.porosity()
                                           * volVars.saturation(phaseIdx);
#if NONISOTHERMAL
            //! The energy storage in the fluid phase with index phaseIdx
            // EnergyLocalResidual::fluidPhaseStorage(storage, scv, volVars, phaseIdx);
            storage[energyEqIdx] += volVars.density(phaseIdx)
                                  * volVars.porosity()
                                  * volVars.internalEnergy(phaseIdx)
                                  * volVars.saturation(phaseIdx);
#endif
        }
#if NONISOTHERMAL
        //! The energy storage in the solid matrix
        EnergyLocalResidual::solidPhaseStorage(storage, scv, volVars);
#endif

        return storage;
    }


    /*!
     * \brief Evaluatex the mass flux over a face of a sub control volume.
     *
     * \param problem The problem
     * \param element The element
     * \param fvGeometry The finite volume geometry context
     * \param elemVolVars The volume variables for all flux stencil elements
     * \param scvf The sub control volume face to compute the flux on
     * \param elemFluxVarsCache The cache related to flux computation
     */
    NumEqVector computeFlux(const Problem& problem,
                            const Element& element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolumeFace& scvf,
                            const ElementFluxVariablesCache& elemFluxVarsCache) const
    {
    	// first assumption: no horizontal flow in interface domain -> flux = 0
        NumEqVector flux;
        flux = 0;
        return flux;
    }

private:
    std::vector<Scalar> dropVolume_;
};

} // end namespace Dumux

#endif
