// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup StokesDropsDarcyCoupling
 * \copydoc Dumux::StokesDropsDarcyDropManager
 */

#ifndef DUMUX_STOKES_DROPS_DARCY_DROPMANAGER_HH
#define DUMUX_STOKES_DROPS_DARCY_DROPMANAGER_HH

#include <dumux/discretization/localview.hh>

namespace Dumux {

/*!
 * \ingroup StokesDropsDarcyCoupling
 * \brief Drop manager to evaluate, update and store drop information.
 */
template<class MDTraits, class CouplingManager>
class StokesDropsDarcyDropManager
{
    using Scalar = typename MDTraits::Scalar;

public:
    static constexpr auto stokesIdx = typename MDTraits::template SubDomain<0>::Index();
    static constexpr auto stokesFaceIdx = typename MDTraits::template SubDomain<1>::Index();
    static constexpr auto interfaceIdx = typename MDTraits::template SubDomain<2>::Index();
    static constexpr auto darcyIdx = typename MDTraits::template SubDomain<3>::Index();

private:
    // obtain the type tags of the sub problems
    using StokesTypeTag = typename MDTraits::template SubDomain<0>::TypeTag;
    using InterfaceTypeTag = typename MDTraits::template SubDomain<2>::TypeTag;
    using DarcyTypeTag = typename MDTraits::template SubDomain<3>::TypeTag;

    using StokesIndices = typename GetPropType<StokesTypeTag, Properties::ModelTraits>::Indices;
    enum { liquidPhaseIdx = 0 };

    using DarcyGridVariables = GetPropType<DarcyTypeTag, Properties::GridVariables>;
    using InterfaceGridVariables = GetPropType<InterfaceTypeTag, Properties::GridVariables>;
    using SolutionVector = typename MDTraits::SolutionVector;
    using NumEqVector = GetPropType<InterfaceTypeTag, Properties::NumEqVector>;

    using DarcyGridView = GetPropType<DarcyTypeTag, Properties::GridView>;
    enum { dim = DarcyGridView::dimension };

    struct DropDomainInfo
    {
        int domainIdx;
        Scalar dropVolume;
        Scalar detachedDropVolume;
        bool dropExists;
        Scalar domainArea; // sum of all elements/scvs in drop domain
        Scalar dropRadius;
        Scalar dropContactArea;
        Scalar aDrop;
        Scalar pmFlux;
    };

    struct PoreClass { Scalar meanPoreRadius; Scalar percentage; };

public:
    //! Constructor
    StokesDropsDarcyDropManager(const CouplingManager& couplingmanager): couplingManager_(couplingmanager)
    { }

    //! Initializes the drop manager
    void init()
    {
        const auto interfaceSpatialParams = couplingManager_.problem(interfaceIdx).spatialParams();

        // get drop domain information from interface problem
        numberOfDropDomains_ = getParam<int>("Interface.Grid.Cells");
        dropDomains_.resize(numberOfDropDomains_);
        computeDropDomains(couplingManager_.problem(interfaceIdx).fvGridGeometry());

        // get drop specific and spatial parameters
        contactAngle_ = interfaceSpatialParams.contactAngle();
        surfaceTension_ = interfaceSpatialParams.surfaceTension();
        porosity_ = interfaceSpatialParams.porosityInterface();

        vMax_ = getParam<Scalar>("Stokes.Problem.Velocity");
        height_ = getParam<std::vector<Scalar>>("Stokes.Grid.UpperRight")[1] - getParam<std::vector<Scalar>>("Stokes.Grid.LowerLeft")[1];
        stokesGridCells_ = getParam<std::vector<Scalar>>("Stokes.Grid.Cells")[0] * getParam<std::vector<Scalar>>("Stokes.Grid.Cells")[1];
        auto poreSizeDistribution = interfaceSpatialParams.poreSizeDistribution();
        for (auto poreClass : poreSizeDistribution)
            poreSizeDistribution_.push_back({poreClass.meanPoreRadius, poreClass.percentage});

        aDrop_.resize(couplingManager_.problem(interfaceIdx).fvGridGeometry().numScv());
        std::fill(aDrop_.begin(), aDrop_.end(), 0.0);

        // compute F_ret - taken from Baber2014a -- only for constant retention force!
        const Scalar contactAngleHysteresis = M_PI / 180 * 20; // #Cho et al 2012 (should be calculated dependent on velocity etc see Kumbur et al.)
        retentionForce_ = surfaceTension_ * (std::cos(M_PI - contactAngle_) + std::cos(contactAngle_ - contactAngleHysteresis)); /* * 1m */

        idxOffset_ = (getParam<std::vector<Scalar>>("Darcy.Grid.Cells")[0] * getParam<std::vector<Scalar>>("Darcy.Grid.Cells")[1])
                   - numberOfDropDomains_;

        std::string outputDropVolumesFile = "dropVolumes.csv";
        outputDropVolumes_.open (outputDropVolumesFile, std::ios_base::app);
    }

    //! Store current time step size
    void setTimeStepSize(const Scalar timeStepSize) const
    { timeStepSize_ = timeStepSize; }

    /*!
     * \brief Assign each element to a 'global' drop domain.
     *
     * Called once (before the time loop).
     */
    template<class InterfaceFVGridGeometry>
    void computeDropDomains(const InterfaceFVGridGeometry& fvGridGeometry)
    {
        const int numberOfElements = fvGridGeometry.numScv();
        auto fvGeometry = localView(fvGridGeometry);

        for (int elementIdx = 0; elementIdx < numberOfElements; ++elementIdx)
        {
            auto element = fvGridGeometry.element(elementIdx);
            fvGeometry.bind(element);
            dropDomains_[elementIdx].domainIdx = elementIdx;
            dropDomains_[elementIdx].domainArea = (*scvs(fvGeometry).begin()).volume();
            waterFluxes_[elementIdx] = 0.0; // initialize water fluxes with zero
        }
    }

    void update(const SolutionVector& sol, const InterfaceGridVariables& interfaceGridVars, const DarcyGridVariables& darcyGridVars) const
    {
        for (auto& dropDomain : dropDomains_)
        {
            dropDomain.detachedDropVolume = 0.0; // reset detached drop volume (was evaluated in previous time step)
            // flux from porous medium into drop under current conditions
            Scalar pmFlux = 0;

            if (!dropDomain.dropExists) // check if drop forms
            {
                const Scalar gasPressureFF = sol[stokesIdx][dropDomain.domainIdx][StokesIndices::pressureIdx - dim];
                const int pmIndex = idxOffset_ + dropDomain.domainIdx;
                const Scalar liquidPressurePM = darcyGridVars.curGridVolVars().volVars(pmIndex).pressure(liquidPhaseIdx);

                // evaluate pressure condition for each pore radius
                for (auto poreClass : poreSizeDistribution_)
                {
                    if (liquidPressurePM >= gasPressureFF + 2*surfaceTension_/poreClass.meanPoreRadius) // if drop forms
                    {
                        const Scalar poreArea = 2 * poreClass.meanPoreRadius; // 2D
                        // elementArea * porosity = A_allPores, A_allPores * percentage = A_poresWithRPore, A_poresWithRPore / poreArea = nPores
                        const Scalar nPores = dropDomain.domainArea * porosity_ * poreClass.percentage / poreArea;
                        // add contribution to global initial drop volume (sum of all caps for current pore size)
                        dropDomain.dropVolume += M_PI * 0.5 * poreClass.meanPoreRadius * poreClass.meanPoreRadius * nPores; // 2D
                    }
                }

                // compute water flux that would flow across the interface under the current pressure conditions
                pmFlux += waterFluxes_.at(dropDomain.domainIdx)
                        * darcyGridVars.curGridVolVars().volVars(dropDomain.domainIdx).porosity()
                        * darcyGridVars.curGridVolVars().volVars(dropDomain.domainIdx).saturation(liquidPhaseIdx);

                // compute water flux that would be necessary to fill the initial drop volume
                const Scalar dropFlux = dropDomain.dropVolume * interfaceGridVars.curGridVolVars().volVars(dropDomain.domainIdx).density(0) / timeStepSize_;

                if (dropDomain.dropVolume > 0 && pmFlux >= dropFlux) // drop forms!
                {
                    dropDomain.dropExists = true;
                    dropDomain.pmFlux = pmFlux;
                    // update drop volume (caused by flux from porous medium, not initial drop volume)
                    dropDomain.dropVolume = pmFlux / interfaceGridVars.curGridVolVars().volVars(dropDomain.domainIdx).density(0) * timeStepSize_;
                    dropDomain.dropRadius = std::sqrt(2 * dropDomain.dropVolume / (contactAngle_ - std::sin(contactAngle_)));
                    dropDomain.dropContactArea = 2 * dropDomain.dropRadius * std::sin(contactAngle_);
                    dropDomain.aDrop = dropDomain.dropContactArea / dropDomain.domainArea;
                    // set drop-covered interface fraction in each element
                    aDrop_[dropDomain.domainIdx] = dropDomain.aDrop;
                }
                else // reset initial drop volume
                {
                    dropDomain.dropVolume = 0.0;
                }
            }
            else // drop does exist -> check if drop grows
            {
                // compute drop growth
                {
                    pmFlux = waterFluxes_.at(dropDomain.domainIdx) * dropDomain.aDrop;
                    dropDomain.pmFlux = pmFlux;
                    dropDomain.dropVolume += pmFlux / interfaceGridVars.curGridVolVars().volVars(dropDomain.domainIdx).density(0) * timeStepSize_;
                    dropDomain.dropRadius = std::sqrt(2 * dropDomain.dropVolume / (contactAngle_ - std::sin(contactAngle_)));
                    dropDomain.dropContactArea = 2 * dropDomain.dropRadius * std::sin(contactAngle_);
                    dropDomain.aDrop = dropDomain.dropContactArea / dropDomain.domainArea;
                    // set drop-covered interface fraction in each element
                    aDrop_[dropDomain.domainIdx] = dropDomain.aDrop;
                }
                // check if drop detaches, if yes reset drop info
                checkDropDetachment(dropDomain, sol, interfaceGridVars);
            }
        }
    }

    //! Checks if a drop detaches in the given drop domain, update drop volume if detached
    void checkDropDetachment(DropDomainInfo& dropDomain,
                             const SolutionVector& solCurrentIter,
                             const InterfaceGridVariables& interfaceGridVars) const
    {

        if (vMax_ > 1.0e-8) // detachment due to drag force, vMax must be > 0
        {
            // compute F_drag - taken from Baber2014a "simple drag force"
            const Scalar firstVerticalFaceIndex = stokesGridCells_ * 1.5 + dropDomain.domainIdx;
            const Scalar vx = solCurrentIter[stokesFaceIdx][firstVerticalFaceIndex];
            const Scalar density = interfaceGridVars.curGridVolVars().volVars(dropDomain.domainIdx).density(liquidPhaseIdx);
            const Scalar viscosity = interfaceGridVars.curGridVolVars().volVars(dropDomain.domainIdx).viscosity(liquidPhaseIdx);

            Scalar projectedArea = dropDomain.dropRadius * (1 - std::cos(contactAngle_));
            Scalar Re = (0.5 * vMax_ * height_ * density) / viscosity;
            Scalar dragCoefficient = 30 / std::sqrt(Re);
            Scalar dragForce = 0.5 * density * vx * vx * dragCoefficient * projectedArea;

            // compare forces
            if (dragForce > retentionForce_)
                resetDropDomainInfo(dropDomain.domainIdx);

        }
        else // V_drop > V_max
        {
            Scalar rPore = dropDomain.domainArea * dropDomain.aDrop * 0.5;
            // V_max = pi * r_pore^2 (2D)
            Scalar Vmax = M_PI * rPore * rPore;

            if (dropDomain.dropVolume > Vmax)
                resetDropDomainInfo(dropDomain.domainIdx);
        }
    }

    //! Writes drop volumes and water fluxes for current time step
    void writeOutput(const Scalar time) const
    {
      outputDropVolumes_ << time;
      for (const auto dropDomain : dropDomains_)
        outputDropVolumes_ << " " << dropDomain.dropVolume;
      outputDropVolumes_ << std::endl;
    }

    Scalar aDrop(const int elementIndex) const
    { return aDrop_[elementIndex]; }

    //! Resets drop domain information when drop detaches
    void resetDropDomainInfo(const int dropDomainIdx) const
    {
        auto& dropDomain = dropDomains_[dropDomainIdx];
        dropDomain.dropExists = false;
        dropDomain.detachedDropVolume = dropDomain.dropVolume;
        dropDomain.dropVolume = 0;
        dropDomain.dropRadius = 0;
        dropDomain.aDrop = 0;
        aDrop_[dropDomain.domainIdx] = 0;
    }

    NumEqVector detachedDrops(const int elementIndex, const Scalar density, const Scalar massFrac, const Scalar internalEnergy) const
    {
        NumEqVector detachedDrops(0.0);
        if (dropDomains_[elementIndex].detachedDropVolume != 0)
        {
            // detached mass (divide by time step size to fit unit of problem.source())
            const Scalar detachedDropsMass = density * dropDomains_[elementIndex].detachedDropVolume / timeStepSize_;
            detachedDrops[0] = detachedDropsMass * massFrac;
            detachedDrops[1] = detachedDropsMass * (1 - massFrac);

#if NONISOTHERMAL
            // detached energy
            const Scalar detachedDropsEnergy = density * internalEnergy * dropDomains_[elementIndex].detachedDropVolume / timeStepSize_;
            detachedDrops[2] = detachedDropsEnergy;
#endif
        }
        return detachedDrops;
    }

    const Scalar getAreaFraction(const int elementIdx) const
    { return dropDomains_[elementIdx].aDrop; }

    template<class WaterFlux>
    void setCurrentWaterFlux(const int elementIdx, const WaterFlux& waterFlux) const
    { waterFluxes_.at(elementIdx) = waterFlux; }

private:
    const CouplingManager& couplingManager_;

    int numberOfDropDomains_;
    mutable std::vector<DropDomainInfo> dropDomains_;
    std::vector<PoreClass> poreSizeDistribution_;
    mutable std::vector<Scalar> aDrop_;
    Scalar porosity_;
    Scalar contactAngle_;
    Scalar surfaceTension_;
    Scalar retentionForce_;
    Scalar vMax_;
    Scalar height_;
    Scalar stokesGridCells_;
    Scalar idxOffset_;
    mutable std::unordered_map<std::size_t, Scalar> waterFluxes_;

    mutable Scalar timeStepSize_;

    mutable std::ofstream outputDropVolumes_;
;
};

} // end namespace Dumux

#endif
