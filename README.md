This module contains all required files to reproduce the results published in the contribution "Upscaling of Coupled Free-Flow and Porous-Medium-Flow Processes" to the book:

G. Lamanna, S. Tonini, G.E. Cossali and B. Weigand: "Dropet Interaction and Spray Processes", Springer, Heidelberg, Berlin, 2020.

Installation
============
You need to have gcc/c++ 5 installed to compile and run this module.

The easiest way to install this module and its dependencies is to create a new directory and clone this module:

```
mkdir Ackermann2019a && cd Ackermann2019a
git clone https://git.iws.uni-stuttgart.de/dumux-pub/ackermann2019a.git
```

After that, execute the file [installAckermann2019a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/ackermann2019a/raw/master/installAckermann2019a.sh)

```
chmod u+x ackermann2019a/installAckermann2019a.sh
./ackermann2019a/installAckermann2019a.sh
```

This should automatically download all necessary modules and check out the correct versions.

Finally, run

```
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all
```

To build and run the executables, navigate to the folder ´ackermann2019a/build-cmake/appl/1p2c_2p2c/´:
```
make test_md_boundary_darcy2p2c_interface2p2c_stokes1p2c
./test_md_boundary_darcy2p2c_interface2p2c_stokes1p2c
make test_md_boundary_darcy2p2cni_interface2p2cni_stokes1p2cni
./test_md_boundary_darcy2p2cni_interface2p2cni_stokes1p2cni
```


Dependencies
=======

dune-common		    releases/2.6	1b45bfb24873f80bfc7b419c04dea79f93aef1d7

dune-geometry	    releases/2.6	1eb09fa58d4894ead8df4dd235bd3d19f2b6ac38

dune-grid		    releases/2.6	76f18471498824d49a6cecbfba520b221d9f79ca

dune-istl		    releases/2.6	9698e497743654b6a03c219b2bdfc27b62a7e0b3

dune-localfunctions	releases/2.6	ee794bfdfa3d4f674664b4155b6b2df36649435f

dumux			    releases/3.0	ae8ae87ddc39d02795ef11ad5744c8e85f334a53


Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir Ackermann2019a
cd Ackermann2019a
```

Download the container startup script by running
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/ackermann2019a/-/raw/master/docker_ackermann2019a.sh
```

Open the Docker Container
```bash
bash docker_ackermann2019a.sh open
```

After the script has run successfully, you may build all executables

```bash
cd ackermann2019a/build-cmake
make build_tests
```

and you can run them individually. They are located in the build-cmake/appl/1p2c_2p2c folder. They can be executed by running

```bash
cd appl/1p2c_2p2c
./test_md_boundary_darcy2p2c_interface2p2c_stokes1p2c
./test_md_boundary_darcy2p2cni_interface2p2cni_stokes1p2cni
```
